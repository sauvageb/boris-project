package com.formation.boris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLException;
import java.util.Scanner;

@SpringBootApplication
public class BorisProjectApplication {

    public static void main(String[] args) throws SQLException {
        SpringApplication.run(BorisProjectApplication.class, args);

        // Ouverture d'un flux
        try (Scanner input = new Scanner(System.in)) {
            input.nextInt();
        }
        // Pas de fermeture de flux


    }

}
